<?php

namespace Drupal\module_conditions\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Module Status' condition.
 *
 * @Condition(
 *   id = "module_status_condition",
 *   label = @Translation("Module Status"),
 * )
 */
class ModuleStatusCondition extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Extension\ExtensionList $extensionList */
    $extensionList = \Drupal::service('extension.list.module');
    $modules = $extensionList->getList();

    // Sort all modules by their names.
    uasort($modules, ['\Drupal\Core\Extension\ExtensionList', 'sortByName']);

    $options = [NULL => $this->t('-Select a module-')];
    foreach ($modules as $module_id => $module) {
      $options[$module_id] = $module->info['name'];
    }

    $form['module'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a module to validate'),
      '#default_value' => $this->configuration['module'],
      '#options' => $options,
      '#description' => $this->t('Module selected status will be use to evaluate condition.'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['module'] = $form_state->getValue('module');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['module' => ''] + parent::defaultConfiguration();
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {
    if (empty($this->configuration['module']) && !$this->isNegated()) {
      return TRUE;
    }

    $module = $this->configuration['module'];

    /** @var \Drupal\Core\Extension\ExtensionList $extensionList */
    $extensionList = \Drupal::service('extension.list.module');
    $modules = $extensionList->getList();

    return $modules[$module]->status;
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    $module = $this->getContextValue('module');

    /** @var \Drupal\Core\Extension\ExtensionList $extensionList */
    $extensionList = \Drupal::service('extension.list.module');
    $modules = $extensionList->getList();

    $status = ($modules[$module]->status) ? $this->t('Enabled') : $this->t('Disabled');

    return $this->t('The module @module is @status.', [
      '@module' => $module,
      '@status' => $status,
    ]);
  }

}
