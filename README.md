# Module Conditions
This module provides a module status condition plugin for Drupal 8+.
Right now, this is the most useful on the Block Visibility section.
Allows you to set conditions on the block visibility whether a module
is enabled or not.

## Example Conditions:
- Enable a block if a module is enabled (could be negated).
- Disable a block if a module is disabled (could be negated).

## Maintainers
George Anderson (geoanders)
https://www.drupal.org/u/geoanders
